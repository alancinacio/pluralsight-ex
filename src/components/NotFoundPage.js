import React, { Component } from 'react';
import { Link } from 'react-router';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Paper from 'material-ui/Paper';

const styles = {
  container: {
    display: 'flex',
    margin: '150px 250px 150px 250px',
    padding: '50px 100px 50px 100px',
    justifyContent: 'center'
  },
  text: {
    marginBottom: 20,
    fontSize: '20px'
  }
};

class NotFoundPage extends Component {
  render() {
    return (
      <div>
        <Paper style={styles.container} zDepth={2}>
          <Grid>
            <Row center="xs" style={styles.text}>
              404 Page Not Found
            </Row>
            <Row center="xs">
              <Link to="/"> Go back to questions page </Link>
            </Row>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default NotFoundPage;