const promise = require('bluebird');
const joi = require('joi');
const errors = require('./errors');

module.exports.validateRequest = (schema, requestBody) => {
  return new promise(function (resolve, reject) {
    joi.validate(requestBody, schema, { presence: "required", stripUnknown: { objects: true } }, function (err, res) {
      if (err) {
        reject(new errors.ValidationError(err.details[0].message));
      } else {
        resolve(res);
      }
    });
  });
};