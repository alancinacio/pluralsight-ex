import * as types from './actionTypes';
import * as questionsActions from '../actions/questionsActions';
import api from '../api';

export function changePageNumber(number) {
  return { type: types.CHANGE_PAGE, number};
}

export function changeSortParams(sort, order) {
  return { type: types.CHANGE_SORT, sort, order};
}

export function changeSearchTerm(term) {
  return { type: types.CHANGE_SEARCH_TERM, term};
}

export function changePage(number) {
  return function (dispatch, getState) {
    dispatch(changePageNumber(number));
    return api.listQuestions(getState().query)
      .then((resp) => {
        dispatch(questionsActions.getQuestionsSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}

export function changeSort(sort, order) {
  return function (dispatch, getState) {
    dispatch(changeSortParams(sort, order));
    return api.listQuestions(getState().query)
      .then((resp) => {
        dispatch(questionsActions.getQuestionsSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}

export function search(term) {
  return function (dispatch, getState) {
    dispatch(changeSearchTerm(term));
    return api.listQuestions(getState().query)
      .then((resp) => {
        dispatch(questionsActions.getQuestionsSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}