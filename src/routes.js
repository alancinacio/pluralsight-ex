import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import QuestionsPage from './containers/QuestionsPage';
import QuestionsPageNoRedux from './containers/QuestionsPageNoRedux';
import QuestionsEditPage from './containers/QuestionsEditPage';
import NotFoundPage from './components/NotFoundPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={QuestionsPage} />
    <Route path="/noredux" component={QuestionsPageNoRedux} />
    <Route path="/question" component={QuestionsEditPage} />
    <Route path="/question/:id" component={QuestionsEditPage} /> 
    <Route path="*" component={NotFoundPage} />
  </Route>
);