'use strict';

const joi = require('joi');
const errors = require('../../lib/errors');
const respond = require('../../lib/respond')
const validation = require('../../lib/validation');
const sqlHelper = require('../../lib/sql');
const uuid = require('node-uuid');

const schema = joi.object().keys({
  question: joi.string().required().description('A question\'s text.'),
  answer: joi.string().required().description('A question\'s correct answer.'),
  distractors: joi.array().required().description('An array of incorrect answers.')
});

module.exports.create = (event, context) => {
  let requestBody;
  let questionId;

  return validation.validateRequest(schema, JSON.parse(JSON.stringify(event.body)))
    .then((result) => {
      questionId = uuid.v4();
      requestBody = result;

      const question = {
        id: questionId,
        question: requestBody.question,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        answer: requestBody.answer,
        distractors: JSON.stringify(requestBody.distractors.filter(String))
      }

      return sqlHelper.createEntity(question, 'questions');
    })
    .then(() => {
      context.succeed(respond.success({ id: questionId }));
    })
    .catch((error) => {
      if (error.name !== "ValidationError") {
        console.error("There was an error: " + JSON.stringify(error.message));
      }

      context.succeed(respond.error(error));
    });
};
