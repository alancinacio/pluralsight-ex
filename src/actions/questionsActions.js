import * as types from './actionTypes';
import api from '../api';

export function getQuestionsSuccess(questions) {
  return { type: types.GET_QUESTIONS_SUCCESS, questions };
}

export function getQuestionSuccess(question) {
  return { type: types.GET_QUESTION_SUCCESS, question };
}

export function deleteQuestionSuccess(removedItemId) {
  return { type: types.DELETE_QUESTION_SUCCESS, removedItemId};
}

export function createQuestionSuccess(question) {
  return { type: types.CREATE_QUESTION_SUCCESS, question};
}

export function updateQuestionSuccess(question) {
  return { type: types.UPDATE_QUESTION_SUCCESS, question};
}

export function getQuestions() {
  return function (dispatch, getState) {
    return api.listQuestions(getState().query)
      .then((resp) => {
        dispatch(getQuestionsSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}

export function deleteQuestion(id) {
  return function (dispatch) {
    return api.deleteQuestionsById(id)
      .then((resp) => {
        dispatch(deleteQuestionSuccess(id));
      }).catch(err => { throw (err); });
  };
}

export function getQuestion(id) {
  return function (dispatch) {
    return api.getQuestionsById(id)
      .then((resp) => {
        dispatch(getQuestionSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}

export function createQuestion(question) {
  return function(dispatch) {
    return api.createQuestions(question)
      .then((resp) => {
        dispatch(createQuestionSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}

export function saveQuestion(question) {
  return function(dispatch) {
    return api.updateQuestions(question.id, question)
      .then((resp) => {
        dispatch(updateQuestionSuccess(resp.data));
      }).catch(err => { throw (err); });
  };
}
