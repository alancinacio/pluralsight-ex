import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import SvgSortIcon from 'material-ui/svg-icons/content/sort';
import IconButton from 'material-ui/IconButton';

class QuestionsSort extends Component {
  constructor(props) {
    super(props);
  }

  changeSort(orderBy) {
    let sort = this.props.query.sortOrder;
    let order = this.props.query.orderBy;
    if (orderBy === order) {
      sort = (sort === 'asc') ? 'desc' : 'asc';
    }
    this.props.changeSort(sort, orderBy);
  }

  handleOrderBy(order) {
    const newState = this.state;
    const currentSort = this.state.query['sortOrder'];
    newState.query['orderBy'] = order;
    if (order === this.state.query['orderBy']) {
      newState.query['sortOrder'] = (currentSort === 'asc') ? 'desc' : 'asc';
    }
    this.setState(newState);
    this.updateQuestionsList();
  }

  render() {
    return (
    <IconMenu
      iconButtonElement={<IconButton><SvgSortIcon /></IconButton>}
      targetOrigin={{ horizontal: 'left', vertical: 'top' }}
      anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
    >
      <MenuItem primaryText="Updated" onTouchTap={() => this.changeSort('updated')} />
      <MenuItem primaryText="Created" onTouchTap={() => this.changeSort('created')} />
      <MenuItem primaryText="Question" onTouchTap={() => this.changeSort('question')} />
      <MenuItem primaryText="Answer" onTouchTap={() => this.changeSort('answer')} />
    </IconMenu>
  );
  }
}

export default QuestionsSort;