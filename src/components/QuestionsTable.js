import React, { Component } from 'react';
import Moment from 'moment';
import { Link } from 'react-router';
import { Grid, Row, Col } from 'react-flexbox-grid';
import SvgModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import SvgMoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';


const QuestionsTable = ({ questions, deleteQuestion, openEdit }) => {
  return (
    <Row center="xs">
      <Col xs={10}>
        <Table height={'500px'}>
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn tooltip="The Question">Question</TableHeaderColumn>
              <TableHeaderColumn tooltip="The Correct Answer">Answer</TableHeaderColumn>
              <TableHeaderColumn tooltip="Incorrect Answers">Distractors</TableHeaderColumn>
              <TableHeaderColumn tooltip="Last Updated">Updated</TableHeaderColumn>
              <TableHeaderColumn>Edit</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            showRowHover
            displayRowCheckbox={false}>
            {questions.map((row, index) => (
              <TableRow key={index}>
                <TableRowColumn>{row.question}</TableRowColumn>
                <TableRowColumn>{row.answer}</TableRowColumn>
                <TableRowColumn>{row.distractors.join(', ')}</TableRowColumn>
                <TableRowColumn>{Moment(row.updated).format('MMMM Do h:mm a')}</TableRowColumn>
                <TableRowColumn>
                  <IconMenu
                    iconButtonElement={<IconButton><SvgMoreVertIcon /></IconButton>}
                    targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                  >
                    <Link to={`/question/${row.id}`}><MenuItem primaryText="Edit"/></Link>
                    <MenuItem primaryText="Delete" onTouchTap={() => deleteQuestion(row.id)} />
                  </IconMenu>
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Col>
    </Row>
  );
};

export default QuestionsTable;