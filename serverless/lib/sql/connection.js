const mysql = require('promise-mysql');
const errors = require('../errors');

const mysqlConfig = {
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB
};
let connection;

module.exports.connection = (query) => {
  return mysql.createConnection(mysqlConfig)
    .then((conn) => {
      connection = conn;
      return conn.query(query);
    })
    .then((rows) => {
      connection.end();
      return rows;
    })
    .catch((err) => {
      connection.end();
      throw new errors.ProcessingError(err);
    });
};
