The deployed site can be accessed at [here](http://pluralsight-ex.s3-website-us-east-1.amazonaws.com)

To run locally (using previously deployed functions): 

* `npm install`
* `npm start`

To deploy: 

* Set your aws credential environment variables: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`
* Install serverless framework: `npm i -g serverless`
* Deploy functions for the first time: `npm run serverless:deploy`
* Modify `config.js` file:
  - `s3Bucket` should be an s3Bucket in your aws account that is [setup](http://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html) for static hosting
  - `baseUrl` should be the url (minus the function name) returned by your serverless:deploy 
* `npm run build` will compile the front end code, upload it to the s3 bucket, redeploy functions and start a local server that serves the compiled code. 