'use strict';

const squel = require('squel');
const mysql = require('./connection');
const errors = require('../errors')

const createEntity = (params, entity) => {
  const query = squel.insert()
    .into(entity)
    .setFields(params);

  return mysql.connection(query.toString());
};

const bulkCreateEntity = (params, entity) => {
  const query = squel.insert()
    .into(entity)
    .setFieldsRows(params)

  return mysql.connection(query.toString());
};

const getEntityById = (id, entity) => {
  const query = squel.select()
    .from(entity)
    .where('id = ?', id);

  return mysql.connection(query.toString());
};

const deleteEntityById = (id, entity) => {
  const query = squel.delete()
    .from(entity)
    .where('id = ?', id);

  return mysql.connection(query.toString());
};

const updateEntityById = (id, params, entity) => {
  const query = squel.update()
    .table(entity)
    .where('id = ?', id)
    .setFields(params);

  return mysql.connection(query.toString());
};

const listEntities = (params, entity) => {

  const query = squel.select()
    .from(entity)

  if (params.pageSize) {
    query.limit(params.pageSize);
    query.offset((params.page - 1) * params.pageSize);
  }

  if (params.sortOrder === 'desc') {
    query.order(params.orderBy, false)
  } else {
    query.order(params.orderBy, true)
  }

  if (params.query && params.query !== '') {
    query.where(squel.expr()
      .or('question' + " LIKE ? ", "%" + params.query + "%")
      .or('distractors' + " LIKE ? ", "%" + params.query + "%")
      .or('answer' + " LIKE ? ", "%" + params.query + "%"));
  }

  return mysql.connection(query.toString());
};

const countEntities = (params, entity) => {
  const query = squel.select()
    .from(entity)

  if (params.query && params.query !== '') {
    query.where(squel.expr()
      .or('question' + " LIKE ? ", "%" + params.query + "%")
      .or('distractors' + " LIKE ? ", "%" + params.query + "%")
      .or('answer' + " LIKE ? ", "%" + params.query + "%"));
  }

  query.field('count(*)', 'count');

  return mysql.connection(query.toString());
};

module.exports = {
  createEntity,
  bulkCreateEntity,
  getEntityById,
  deleteEntityById,
  updateEntityById,
  listEntities,
  countEntities
}
