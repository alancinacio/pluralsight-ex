const joi = require('joi');
const errors = require('../../lib/errors');
const respond = require('../../lib/respond');
const validation = require('../../lib/validation');
const sqlHelper = require('../../lib/sql');
const uuid = require('node-uuid');

const schema = joi.object().keys({
  queryStringParameters: joi.object().optional().allow(null).keys({
    sortOrder: joi.string().valid('asc', 'desc').optional().description('Order to sort response'),
    page: joi.number().integer().optional().description('Page to retrieve.'),
    pageSize: joi.number().max(50).integer().optional().description('Size of response page.'),
    query: joi.string().optional().description('A string to match the question, answer or distractors table'),
    orderBy: joi.string().valid('question', 'created', 'updated', 'answer').optional().description('Column to order by')
  })
});

module.exports.list = (event, context) => {
  let queryResult;
  let requestQuery;
  const data = {
    questions: [],
    meta: {}
  };

  return validation.validateRequest(schema, event)
    .then((result) => {
      
      if (!result.queryStringParameters) {
        result.queryStringParameters = {};
      }

      requestQuery = {
        pageSize: result.queryStringParameters.pageSize || 50,
        sortOrder: result.queryStringParameters.sortOrder || 'asc',
        page: result.queryStringParameters.page || 1,
        orderBy: result.queryStringParameters.orderBy || 'updated',
        query: result.queryStringParameters.query || ''
      };

      return sqlHelper.countEntities(requestQuery, 'questions');

    })
    .then((resp) => {
      requestQuery.page = (resp[0].count / requestQuery.pageSize) > requestQuery.page ? requestQuery.page : Math.ceil(resp[0].count / requestQuery.pageSize);
      data.meta = {
        count: resp[0].count,
        page: requestQuery.page,
        pageSize: requestQuery.pageSize,
        sortOrder: requestQuery.sortOrder
      };

      if (resp[0].count === 0) {
        context.succeed(respond.success(data));
      } else {
        return sqlHelper.listEntities(requestQuery, 'questions');
      }
    })
    .then((resp) => {
      data.questions = resp.map((item) => {
        return {
          id: item.id,
          question: item.question,
          created: item.created,
          updated: item.updated,
          answer: item.answer,
          distractors: JSON.parse(item.distractors)
        };
      });
      context.succeed(respond.success(data));
    })
    .catch((error) => {
      if (error.name !== "ValidationError") {
        console.error("There was an error: " + JSON.stringify(error.message));
      }
      context.succeed(respond.error(error));
    });
};