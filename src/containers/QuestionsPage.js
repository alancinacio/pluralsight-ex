import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import * as questionsActions from '../actions/questionsActions';
import * as queryActions from '../actions/queryActions';
import { Grid, Row, Col } from 'react-flexbox-grid';
import QuestionsTable from '../components/QuestionsTable';
import QuestionsPagination from '../components/QuestionsPagination';
import QuestionsSearch from '../components/QuestionsSearch';
import QuestionsSort from '../components/QuestionsSort';
import SvgAddCircleIcon from 'material-ui/svg-icons/content/add-circle';
import IconButton from 'material-ui/IconButton';

class NewQuestionsPage extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.actions.getQuestions();
  }

  render() {
    const { questions, meta, actions, query, ui, question } = this.props;

    return (
      <div>
        <QuestionsSearch search={actions.search}/>
        <Row>
          <Col xs={1} xsOffset={10}>
            <QuestionsSort changeSort={actions.changeSort} query={query}/>
          </Col>
          <Col>
            <IconButton tooltip="Add a new question" onTouchTap={() => browserHistory.push('/question')}>
              <SvgAddCircleIcon />
            </IconButton>
          </Col>
        </Row>
        <QuestionsTable questions={questions} deleteQuestion={actions.deleteQuestion}/>
        <QuestionsPagination meta={meta} changePage={actions.changePage} />
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    questions: state.questions.questions,
    meta: state.questions.meta,
    query: state.query,
    question: state.question
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, questionsActions, queryActions), dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewQuestionsPage);