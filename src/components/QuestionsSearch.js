import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import TextField from 'material-ui/TextField';

class QuestionsSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ''
    };
  }

  onChange() {
    return {
      onChange: (event) => {
        const newState = this.state;
        newState.searchTerm = event.target.value;
        this.setState(newState);
      },
      onKeyPress: (event) => {
        if (event.charCode === 13) {
          this.props.search(this.state.searchTerm);
        }
      }
    };
  }

  render() {
    return (
      <Row center="xs">
        <TextField
          hintText="Search"
          value={this.state.searchTerm}
          style={{ marginBottom: 40 }}
          {...this.onChange() }
        />
      </Row>
    );
  }

}

export default QuestionsSearch;