import { Promise } from 'bluebird';
import config from '../../config';
import 'whatwg-fetch';

const baseUrl = config.baseUrl;

class Questions {

  static getQuestionsById(id) {
    return fetch(`${baseUrl}/questions/${id}`)
      .then((resp) => {
        return resp.json();
      })
      .then((body) => {
        if (body.success) {
          return Promise.resolve(body);
        } else {
          return Promise.reject(new Error(body.message));
        }
      });
  }

  static deleteQuestionsById(id) {
    return fetch(`${baseUrl}/questions/${id}`, { method: 'DELETE' })
      .then((resp) => {
        return resp.json();
      })
      .then((body) => {
        if (body.success) {
          return Promise.resolve(body);
        } else {
          return Promise.reject(new Error(body.message));
        }
      });
  }

  static createQuestions(payload) {
    return fetch(`${baseUrl}/questions`, { method: 'POST', body: JSON.stringify(payload) })
      .then((resp) => {
        return resp.json();
      })
      .then((body) => {
        if (body.success) {
          return Promise.resolve(body);
        } else {
          return Promise.reject(new Error(body.message));
        }
      });
  }

  static updateQuestions(id, payload) {
    return fetch(`${baseUrl}/questions/${id}`, { method: 'PUT', body: JSON.stringify(payload) })
      .then((resp) => {
        return resp.json();
      })
      .then((body) => {
        if (body.success) {
          return Promise.resolve(body);
        } else {
          return Promise.reject(new Error(body.message));
        }
      });
  }

  static listQuestions(params) {
    let url = `${config.baseUrl}/questions`;
    if (params) {
      url = url + '?';
      for (let key in params) {
        if (params[key]) {
          url = url + key + '=' + params[key] + '&';
        }
      }
    }
    return fetch(url)
      .then((resp) => {
        return resp.json();
      })
      .then((body) => {
        if (body.success) {
          return body;
        } else {
          new Error(body.message);
        }
      });
  }

}

export default Questions;
