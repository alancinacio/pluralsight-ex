'use strict';

const joi = require('joi');
const errors = require('../../lib/errors');
const respond = require('../../lib/respond')
const validation = require('../../lib/validation');
const sqlHelper = require('../../lib/sql');
const uuid = require('node-uuid');

const schema = joi.object().keys({
  pathParameters: joi.object().keys({
    id: joi.string().guid().required().description('An Entity ID as a valid guid.')
  }),
  body: joi.object().keys({
    question: joi.string().required().description('A question\'s text.'),
    answer: joi.string().required().description('A question\'s correct answer.'),
    distractors: joi.array().required().description('A question\'s incorrect answers.')
  })
});

module.exports.update = (event, context) => {
  let requestBody;

  return validation.validateRequest(schema, event)
    .then((result) => {
      requestBody = result;

      const question = {
        question: requestBody.body.question,
        answer: requestBody.body.answer,
        distractors: JSON.stringify(requestBody.body.distractors.filter(String)),
        updated: new Date().toISOString()
      }

      return sqlHelper.updateEntityById(requestBody.pathParameters.id, question, 'questions');
    })
    .then((resp) => {
      context.succeed(respond.success());
    })
    .catch((error) => {
      if (error.name !== "ValidationError") {
        console.error("There was an error: " + JSON.stringify(error.message));
      }

      context.succeed(respond.error(error));
    });
};
