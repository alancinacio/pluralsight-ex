const success = (message) => {
  const responseMessage = { success: true };
  responseMessage['data'] = message;
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : "*"
    },
    body: JSON.stringify(responseMessage)
  };
};

const error = (error) => {
  return {
    statusCode: error.code,
    headers: {
      "Access-Control-Allow-Origin" : "*"
    },
    body: JSON.stringify(error)
  };
};

module.exports = {
  success,
  error
};