import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function questionsReducer(state = initialState.questions, action) {
  
  switch(action.type) {
    case types.GET_QUESTIONS_SUCCESS: {
      const newState = Object.assign({}, state, { questions: action.questions.questions, meta: action.questions.meta});
      return newState;
    }
    case types.DELETE_QUESTION_SUCCESS: {
      const newState = Object.assign({}, state, { questions: state.questions.filter(item => action.removedItemId !== item.id)});
      return newState;
    }
    default: 
      return state;
  }
}