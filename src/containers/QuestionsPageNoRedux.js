import React, { Component } from 'react';
import { Link } from 'react-router';
import Moment from 'moment';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Pagination from 'material-ui-pagination';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SvgModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import SvgMoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import SvgAddCircleIcon from 'material-ui/svg-icons/content/add-circle';
import SvgSortIcon from 'material-ui/svg-icons/content/sort';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import Paper from 'material-ui/Paper';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

import Questions from '../api';


const styles = {
  searchButton: {
    margin: 20
  }
};

class QuestionsPageNoRedux extends Component {
  constructor(props) {
    super(props);

    this.state = {
      questionsList: [],
      totalPages: 0,
      currentPage: 0,
      searchTerm: '',
      editOpen: false,
      tempQuestion: {
        id: '',
        question: '',
        answer: '',
        distractors: ['','']
      },
      query: {
        pageSize: 10,
        orderBy: 'updated',
        sortOrder: 'desc'
      }
    };

    this.updateQuestionsList = this.updateQuestionsList.bind(this);
  }

  componentWillMount() {
    this.updateQuestionsList();
  }

  updateQuestionsList() {
    let total;
    Questions.listQuestions(this.state.query)
      .then((resp) => {
        if (resp.success) {
          const newState = this.state;
          newState.questionsList = resp.data.questions;
          newState.currentPage = resp.data.meta.page;
          newState.totalPages = resp.data.meta.count / resp.data.meta.pageSize;
          newState.editOpen = false,
            newState.tempQuestion = {
              id: '',
              question: '',
              answer: '',
              distractors: ['','']
            };
          this.setState(newState);
        }
      });
  }

  onSearchChangeControl() {
    return {
      onChange: (event) => {
        const newState = this.state;
        newState.searchTerm = event.target.value;
        newState.query.query = event.target.value;
        this.setState(newState);
      },
      onKeyPress: (event) => {
        if (event.charCode === 13) {
          this.updateQuestionsList();
        } 
      }
    };
  }

  onTempChangeControl(stateProperty, index) {
    return {
      onChange: (event) => {
        const newState = this.state;
        if (index) {
          newState.tempQuestion[stateProperty][index - 1] = event.target.value;
        } else {
          newState.tempQuestion[stateProperty] = event.target.value;
        }
        this.setState(newState);
      }
    };
  }

  handleDelete(item) {
    return {
      onTouchTap: (event) => {
        Questions.deleteQuestionsById(item.id)
          .then((resp) => {
            this.updateQuestionsList();
          });
      }
    };
  }

  handleEditDialogOpen(change, item) {
    const newState = this.state;
    newState.editOpen = change;
    if (item) {
      newState.tempQuestion = {
        question: item.question,
        answer: item.answer,
        distractors: item.distractors,
        id: item.id
      };
    } else {
      newState.tempQuestion = {
        question: '',
        answer: '',
        distractors: ['','']
      };
    }
    this.setState(newState);
  }

  handleSaveQuestion() {
    if (this.state.tempQuestion.id) {
      Questions.updateQuestions(this.state.tempQuestion.id, this.state.tempQuestion)
        .then((resp) => {
          this.updateQuestionsList();
        });
    } else {
      Questions.createQuestions(this.state.tempQuestion)
        .then((resp) => {
          this.updateQuestionsList();
        });
    }
  }

  changePage(number) {
    const newState = this.state;
    newState.query['page'] = number;
    this.setState(newState);
    this.updateQuestionsList();
  }

  addDistractor() {
    const newState = this.state;
    newState.tempQuestion.distractors.push('');
    this.setState(newState);
  }

  handleOrderBy(order) {
    const newState = this.state;
    const currentSort = this.state.query['sortOrder'];
    newState.query['orderBy'] = order;
    if (order === this.state.query['orderBy']) {
      newState.query['sortOrder'] = (currentSort === 'asc') ? 'desc' : 'asc';
    }
    this.setState(newState);
    this.updateQuestionsList();
  }

  render() {

    const editForm = (
      <div>
        <TextField
          style={styles.fields}
          hintText="Your Question Text"
          floatingLabelText="Question"
          value={this.state.tempQuestion.question}
          {...this.onTempChangeControl('question') }
          fullWidth
        />
        <TextField
          style={styles.fields}
          hintText="Your Question's Answer"
          floatingLabelText="Answer"
          value={this.state.tempQuestion.answer}
          {...this.onTempChangeControl('answer') }
          fullWidth
        />
        {this.state.tempQuestion.distractors && this.state.tempQuestion.distractors.map((distractor, index) => {
          return (<TextField
            key={index}
            style={styles.fields}
            hintText="Your Question's Distractors"
            floatingLabelText="Distractor"
            value={this.state.tempQuestion.distractors[index]}
            {...this.onTempChangeControl('distractors', index + 1) }
            fullWidth
          />);
        })}

        <IconButton tooltip="Add another distractor"
          onTouchTap={() => this.addDistractor()}>
          <SvgAddCircleIcon />
        </IconButton>


      </div>
    );

    const dialogActions = [
      <RaisedButton
        key="cancel"
        label="Cancel"
        onTouchTap={() => this.handleEditDialogOpen(false)}
      />,
      <RaisedButton
        key={"save"}
        label="Save"
        keyboardFocused
        primary
        onTouchTap={() => this.handleSaveQuestion()}
        style={{ marginLeft: 10 }}
      />
    ];

    return (
      <Grid>
        <Row center="xs">
          <TextField
            hintText="Search"
            {...this.onSearchChangeControl() }
          />
        </Row>
        <Row>
          <Col xs={1} xsOffset={10}>
            <IconMenu
              iconButtonElement={<IconButton><SvgSortIcon /></IconButton>}
              targetOrigin={{ horizontal: 'left', vertical: 'top' }}
              anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
            >
              <MenuItem primaryText="Updated" onTouchTap={() => this.handleOrderBy('updated')} />
              <MenuItem primaryText="Created" onTouchTap={() => this.handleOrderBy('created')} />
              <MenuItem primaryText="Question" onTouchTap={() => this.handleOrderBy('question')} />
              <MenuItem primaryText="Answer" onTouchTap={() => this.handleOrderBy('answer')} />
            </IconMenu>
          </Col>
          <Col>
            <IconButton tooltip="Add a new question"
              onTouchTap={() => this.handleEditDialogOpen(true)}>
              <SvgAddCircleIcon />
            </IconButton>
          </Col>
        </Row>
        <Row center="xs">
          <Col xs={10}>
            <Table height={'500px'}>
              <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn tooltip="The Question">Question</TableHeaderColumn>
                  <TableHeaderColumn tooltip="The Correct Answer">Answer</TableHeaderColumn>
                  <TableHeaderColumn tooltip="Incorrect Answers">Distractors</TableHeaderColumn>
                  <TableHeaderColumn tooltip="Last Updated">Updated</TableHeaderColumn>
                  <TableHeaderColumn>Edit</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody
                showRowHover
                displayRowCheckbox={false}>
                {this.state.questionsList.map((row, index) => (
                  <TableRow key={index}>
                    <TableRowColumn>{row.question}</TableRowColumn>
                    <TableRowColumn>{row.answer}</TableRowColumn>
                    <TableRowColumn>{row.distractors.join(', ')}</TableRowColumn>
                    <TableRowColumn>{Moment(row.updated).format('MMMM Do h:mm a')}</TableRowColumn>
                    <TableRowColumn>
                      <IconMenu
                        iconButtonElement={<IconButton><SvgMoreVertIcon /></IconButton>}
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                      >
                        <MenuItem primaryText="Edit" onTouchTap={() => this.handleEditDialogOpen(true, row)} />
                        <MenuItem primaryText="Delete" {...this.handleDelete(row) } />
                      </IconMenu>
                    </TableRowColumn>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Col>
        </Row>
        {this.state.totalPages >= 1 && <Row center="xs">
          <div style={{ marginTop: 40 }}>
            <Pagination
              total={this.state.totalPages}
              current={this.state.currentPage}
              display={7}
              onChange={number => this.changePage(number)}
            />
          </div>
        </Row>}

        <Dialog
          title="Question"
          actions={dialogActions}
          modal={false}
          open={this.state.editOpen}
          onRequestClose={() => this.handleEditDialogOpen(false)}
        >
          {editForm}
        </Dialog>

      </Grid>
    );
  }
}

export default QuestionsPageNoRedux;