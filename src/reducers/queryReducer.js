import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function queryReducer(state = initialState.query, action) {
  switch(action.type) {
    case types.CHANGE_PAGE: {
      const newState = Object.assign({}, state, {page: action.number});
      return newState;
    }
    case types.CHANGE_SORT: {
      const newState = Object.assign({}, state, {orderBy: action.order, sortOrder: action.sort});
      return newState;
    }
    case types.CHANGE_SEARCH_TERM: {
      const newState = Object.assign({}, state, {query: action.term});
      return newState;
    }
    default: 
      return state;
  }
}