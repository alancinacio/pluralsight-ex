const joi = require('joi');
const errors = require('../../lib/errors');
const respond = require('../../lib/respond')
const validation = require('../../lib/validation');
const sqlHelper = require('../../lib/sql');

const schema = joi.object().keys({
    pathParameters: joi.object().keys({
        id: joi.string().guid().required().description('A question ID as a valid guid.')
    })
});

module.exports.get = (event, context) => {
    let questionId;

    return validation.validateRequest(schema, event)
        .then((result) => {
            return sqlHelper.getEntityById(result.pathParameters.id, 'questions');
        })
        .then((rows) => {
            if (rows.length === 0) {
                throw new errors.SourceItemNotFoundError();
            }

            const question = {
                question: {
                    id: rows[0].id,
                    question: rows[0].question,
                    created: rows[0].created,
                    updated: rows[0].updated,
                    answer: rows[0].answer,
                    distractors: JSON.parse(rows[0].distractors) || []
                }
            }
            context.succeed(respond.success(question));
        })
        .catch((error) => {
            if (error.name !== "ValidationError") {
                console.error("There was an error: " + JSON.stringify(error.message));
            }
            context.succeed(respond.error(error));
        });

};
