export default {
  questions: {
    questions: [],
    meta: {}
  },
  query: {
    pageSize: 10,
    orderBy: 'updated',
    sortOrder: 'desc',
    page: 1
  },
  question: {
    id: '',
    question: '',
    answer: '',
    distractors: ['', '']
  }
};