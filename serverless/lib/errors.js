function ValidationError(message) {
  Error.call(this, message);

  this.message = message || "Bad Request: You submitted an invalid input.";
  this.name = "ValidationError";
  this.code = 400;
}
ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

function ProcessingError(message) {
  const error = Error.call(this, message);

  this.message = message || "Oops, something went wrong while trying to process your request.";
  this.name = "ProcessingError";
  this.code = 500;
  this.stack = error.stack;
}
ProcessingError.prototype = Object.create(Error.prototype);
ProcessingError.prototype.constructor = ProcessingError;

function UnauthorizedError(message) {
  const error = Error.call(this, message);

  this.message = message || "Unauthorized: You are not authorized to view this resource.";
  this.name = "Unauthorized";
  this.code = 401;
  this.stack = error.stack;
}
UnauthorizedError.prototype = Object.create(Error.prototype);
UnauthorizedError.prototype.constructor = UnauthorizedError;


function SourceItemNotFoundError(message) {
  const error = Error.call(this, message);

  this.message = message || "Not Found: The specified source item was not found.";
  this.name = "NotFound";
  this.code = 404;
}
SourceItemNotFoundError.prototype = Object.create(Error.prototype);
SourceItemNotFoundError.prototype.constructor = SourceItemNotFoundError;

module.exports = {
    ValidationError: ValidationError,
    ProcessingError: ProcessingError,
    UnauthorizedError: UnauthorizedError,
    SourceItemNotFoundError: SourceItemNotFoundError
};