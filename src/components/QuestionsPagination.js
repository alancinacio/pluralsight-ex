import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Pagination from 'material-ui-pagination';


const QuestionsPagination = ({ meta, changePage }) => {
  return (
    <Row center="xs">
      {meta.count / meta.pageSize >= 1 &&
        <div style={{ marginTop: 40 }}>
          <Pagination
            total={meta.count / meta.pageSize}
            current={meta.page}
            display={7}
            onChange={number => changePage(number)}
          />
        </div>
      }
    </Row>
  );
};

export default QuestionsPagination;