import { combineReducers } from 'redux';
import questions from './questionsReducer';
import question from './questionReducer';
import query from './queryReducer';

const rootReducer = combineReducers({questions, question, query});

export default rootReducer;