import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function questionReducer(state = initialState.question, action) {
  switch (action.type) {
    case types.GET_QUESTION_SUCCESS: {
      const newState = Object.assign({}, state, action.question.question);
      return newState;
    }
    case types.CREATE_QUESTION_SUCCESS: {
      const newState = Object.assign({}, state, {id: action.id});
      return newState;
    }
    case types.UPDATE_QUESTION_SUCCESS: {
      const newState = Object.assign({}, state, {id: action.id});
      return newState;
    }
    default:
      return state;
  }
}