'use strict';

const joi = require('joi');
const errors = require('../../lib/errors');
const respond = require('../../lib/respond')
const validation = require('../../lib/validation');
const sqlHelper = require('../../lib/sql');

const schema = joi.object().keys({
  pathParameters: joi.object().keys({
    id: joi.string().guid().required().description('An Entity ID as a valid guid.')
  })
});

module.exports.delete = (event, context) => {
  let questionId;

  return validation.validateRequest(schema, event)
    .then((result) => {
      return sqlHelper.deleteEntityById(result.pathParameters.id, 'questions');
    })
    .then((rows) => {
      if (rows.affectedRows === 0) {
        throw new errors.SourceItemNotFoundError();
      }

      context.succeed(respond.success());
    })
    .catch((error) => {
      if (error.name !== "ValidationError") {
        console.error("There was an error: " + JSON.stringify(error.message));
      }
      context.succeed(respond.error(error));
    });

};
