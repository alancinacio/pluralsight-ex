import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as questionsActions from '../actions/questionsActions';
import { Grid, Row, Col } from 'react-flexbox-grid';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import SvgAddCircleIcon from 'material-ui/svg-icons/content/add-circle';
import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router';
import AssignDeep from 'object-assign-deep';

class QuestionsEditPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      question: AssignDeep({}, this.props.question)
    };
  }

  componentDidMount() {
    if (this.props.id) {
      this.props.actions.getQuestion(this.props.id).then(() => {
        this.setState({ question: AssignDeep({}, this.props.question)});
      });
    } else {
      this.setState({ question: AssignDeep({}, this.props.emptyQuestion)});
    }
  }

  onFieldChange(name, index) {
    return {
      onChange: (event) => {
        const newState = this.state;      
        if (index) {
          newState.question[name][index - 1] = event.target.value;
        } else {
          newState.question[name] = event.target.value;
        }
        this.setState(newState);
      }
    };
  }

  onAddDistractor() {
    const newState = this.state;
    newState.question.distractors.push('');
    this.setState(newState);
  }

  onSave() {
    if (this.state.question.id) {
      this.props.actions.saveQuestion(this.state.question)
        .then(() => {browserHistory.push('/'); });
    } else {
      this.props.actions.createQuestion(Object.assign({}, this.state.question))
        .then(() => {browserHistory.push('/'); });
    }
  }

  render() {
    return (
      <div>
        <Row center="xs">
          <Col xs={6}>
            <TextField
              hintText="Your Question Text"
              floatingLabelText="Question"
              value={this.state.question.question}
              {...this.onFieldChange('question') }
              fullWidth
            />
            <TextField
              hintText="Your Question's Answer"
              floatingLabelText="Answer"
              value={this.state.question.answer}
              {...this.onFieldChange('answer') }
              fullWidth
            />
            {this.state.question.distractors && this.state.question.distractors.map((distractor, index) => {
              return (<TextField
                key={index}
                hintText="Your Question's Distractors"
                floatingLabelText="Distractor"
                value={this.state.question.distractors[index]}
                {...this.onFieldChange('distractors', index + 1) }
                fullWidth
              />);
            })}
            <IconButton tooltip="Add another distractor"
              onTouchTap={() => this.onAddDistractor()}>
              <SvgAddCircleIcon />
            </IconButton>
          </Col>
        </Row>
        <Row center="xs" style={{ marginTop: 40 }}>
          <Col>
            <RaisedButton
              key="cancel"
              label="Cancel"
              onTouchTap={() => browserHistory.push('/')}
            />
            <RaisedButton
              key="save"
              label="Save"
              keyboardFocused
              primary
              onTouchTap={() => this.onSave()}
              style={{ marginLeft: 10 }}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    id: ownProps.params.id,
    question: state.question,
    emptyQuestion: {
      id: '',
      question: '',
      answer: '',
      distractors: ['', '']
    }
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(questionsActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsEditPage);